SELECT name,rating from pizzeria
where rating >=3.5 and 
      rating <=5.0
ORDER BY rating DESC;

SELECT name,rating from pizzeria
where rating BETWEEN 3.5 and 5.0
ORDER BY rating DESC;
--The BETWEEN operator is inclusive: begin and end values are included.