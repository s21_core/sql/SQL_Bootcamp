SELECT (case WHEN P.id = V.person_id THEN P.name ELSE 'unknown' end ) as person_name,
        (case WHEN Z.id = V.pizzeria_id THEN Z.name ELSE 'unknown' end ) as pizzeria_name
from person P,pizzeria Z,(SELECT person_id, pizzeria_id FROM person_visits WHERE visit_date >= '2022-01-07' and visit_date <= '2022-01-09') as V
WHERE Z.id = V.pizzeria_id and P.id = V.person_id
ORDER BY person_name ASC, pizzeria_name DESC;