SELECT action_date, name 
from person as P,
    (SELECT  order_date as action_date, person_id  FROM person_order 
    INTERSECT
    SELECT  visit_date as action_date, person_id FROM person_visits 
    ) as UN
where UN.person_id = P.id
ORDER BY action_date ASC, P.name DESC