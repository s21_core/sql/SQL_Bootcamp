SELECT O.order_date,
       concat(name,' (age:',age, ')') as person_information
from person P
JOIN person_order as O ON O.person_id=P.id
ORDER BY O.order_date ASC,
         P.name ASC