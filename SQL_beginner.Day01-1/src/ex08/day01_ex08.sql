SELECT O.order_date, concat(name,' (age:',age, ')') as person_information from person P
NATURAL JOIN (SELECT person_id as id, order_date From person_order) AS O
ORDER BY O.order_date ASC, P.name ASC