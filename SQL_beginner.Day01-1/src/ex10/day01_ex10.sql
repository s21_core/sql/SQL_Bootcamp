SELECT P.name as person_name,M.pizza_name as pizza_name,Z.name as pizzeria_name from person_order O 
JOIN person P on p.id = o.person_id
JOIN menu M on M.id = O.menu_id
JOIN pizzeria Z on Z.id = M.pizzeria_id
ORDER BY person_name, pizza_name, pizzeria_name