SELECT name, rating FROM pizzeria P
FULL OUTER JOIN person_visits V
on v.pizzeria_id = p.id
where v.pizzeria_id is NULL;
SELECT DI.DATE as missing_date
FROM GENERATE_SERIES('2022-01-01', '2022-01-10', interval '1 day') as DI
LEFT JOIN 
    (SELECT V2.visit_date
    FROM
        (SELECT visit_date
        FROM person_visits
        where person_id = 2) as V2
    FULL OUTER JOIN
        (SELECT visit_date
        FROM person_visits
        where person_id = 1) as V1 ON V1.visit_date = V2.visit_date) as V
ON DI.DATE=V.visit_date
Where V.visit_date is NULL;
SELECT COALESCE(P.name,'-') as person_name, V.visit_date as visit_date, COALESCE(Z.name,'-') as pizzeria_name FROM 
    (SELECT * from person_visits WHERE visit_date BETWEEN '2022-01-01' AND '2022-01-03') as V
FULL JOIN person P
on V.person_id= p.id 
FULL JOIN pizzeria Z
ON Z.id=V.pizzeria_id
ORDER BY person_name,visit_date,pizzeria_name;



WITH cte(missing_date) AS 
    ((SELECT DI.DATE as missing_date
    FROM GENERATE_SERIES('2022-01-01', '2022-01-10', interval '1 day') as DI))
SELECT missing_date from cte
LEFT JOIN 
    (SELECT V2.visit_date
    FROM
        (SELECT visit_date
        FROM person_visits
        where person_id = 2) as V2
    FULL OUTER JOIN
        (SELECT visit_date
        FROM person_visits
        where person_id = 1) as V1 ON V1.visit_date = V2.visit_date) as V
ON missing_date=V.visit_date
Where V.visit_date is NULL;
SELECT M.pizza_name, Z.name as pizza_name, M.price as price FROM pizzeria as Z
LEFT JOIN 
   (SELECT pizza_name,pizzeria_id,price FROM menu
   WHERE pizza_name='mushroom pizza' or pizza_name='pepperoni pizza') as M
   ON M.pizzeria_id=Z.id WHERE M.pizza_name IS NOT NULL
ORDER BY M.pizza_name,Z.name;
SELECT name from person
WHERE age >= 25 and gender='female'
ORDER BY name;



SELECT M.pizza_name, Z.name FROM person_order as O
JOIN menu M on O.menu_id=M.id 
JOIN person P on O.person_id=P.id
Join pizzeria as Z ON Z.id=M.pizzeria_id
WHERE P.name = 'Anna' OR P.name = 'Denis'
ORDER BY 1,2;
SELECT Z.name as pizzeria_name
FROM person_visits V
JOIN person P ON V.person_id = P.id
JOIN menu M ON V.pizzeria_id = M.pizzeria_id
JOIN pizzeria Z ON M.pizzeria_id= Z.id
WHERE P.name = 'Dmitriy'
    AND V.visit_date = '2022-01-08'
    AND M.price < 800;
SELECT DISTINCT P.name
FROM person_order O
JOIN 
    (SELECT id, name from person 
    WHERE gender='male'
    and (address='Moscow' or address='Samara')) as P
ON O.person_id = P.id
JOIN menu M ON O.menu_id = M.id
WHERE M.pizza_name IN ('pepperoni pizza', 'mushroom pizza');
with cte_female_pep_che as
(SELECT P.name, M1.pizza_name from person_order O
JOIN (SELECT * FROM person
      WHERE gender='female') P ON p.id=O.person_id 
JOIN (SELECT * FROM menu
      WHERE pizza_name = 'pepperoni pizza' or pizza_name = 'cheese pizza')as M1 ON M1.id=O.menu_id
)
SELECT DISTINCT cte1.name From cte_female_pep_che as cte1
inner JOIN cte_female_pep_che as cte2 ON cte1.name = cte2.name and cte1.pizza_name!=cte2.pizza_name;
SELECT P1.name AS person_name1,
       P2.name AS person_name2,
       P1.address AS common_address
FROM person AS P1
CROSS JOIN person AS P2
WHERE P1.name != P2.name
    AND P1.address = P2.address
    AND P1.id > P2.id
ORDER BY person_name1,
         person_name2,
         common_address;
