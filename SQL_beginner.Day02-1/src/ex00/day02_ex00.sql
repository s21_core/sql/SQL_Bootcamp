SELECT name, rating FROM pizzeria P
FULL OUTER JOIN person_visits V
on v.pizzeria_id = p.id
where v.pizzeria_id is NULL;
