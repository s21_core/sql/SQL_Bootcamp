WITH cte(missing_date) AS 
    ((SELECT DI.DATE as missing_date
    FROM GENERATE_SERIES('2022-01-01', '2022-01-10', interval '1 day') as DI))
SELECT missing_date from cte
LEFT JOIN 
    (SELECT V2.visit_date
    FROM
        (SELECT visit_date
        FROM person_visits
        where person_id = 2) as V2
    FULL OUTER JOIN
        (SELECT visit_date
        FROM person_visits
        where person_id = 1) as V1 ON V1.visit_date = V2.visit_date) as V
ON missing_date=V.visit_date
Where V.visit_date is NULL;