SELECT M.pizza_name, Z.name as pizzeria_name, M.price as price FROM pizzeria as Z
LEFT JOIN 
   (SELECT pizza_name,pizzeria_id,price FROM menu
   WHERE pizza_name='mushroom pizza' or pizza_name='pepperoni pizza') as M
   ON M.pizzeria_id=Z.id WHERE M.pizza_name IS NOT NULL
ORDER BY M.pizza_name,Z.name;