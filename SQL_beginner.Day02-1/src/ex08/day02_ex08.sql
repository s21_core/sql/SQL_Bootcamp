SELECT DISTINCT P.name
FROM person_order O
JOIN 
    (SELECT id, name from person 
    WHERE gender='male'
    and (address='Moscow' or address='Samara')) as P
ON O.person_id = P.id
JOIN menu M ON O.menu_id = M.id
WHERE M.pizza_name IN ('pepperoni pizza', 'mushroom pizza');
