with cte_female_pep_che as
(SELECT P.name, M1.pizza_name from person_order O
JOIN (SELECT * FROM person
      WHERE gender='female') P ON p.id=O.person_id 
JOIN (SELECT * FROM menu
      WHERE pizza_name = 'pepperoni pizza' or pizza_name = 'cheese pizza')as M1 ON M1.id=O.menu_id
)
SELECT DISTINCT cte1.name From cte_female_pep_che as cte1
inner JOIN cte_female_pep_che as cte2 ON cte1.name = cte2.name and cte1.pizza_name!=cte2.pizza_name;