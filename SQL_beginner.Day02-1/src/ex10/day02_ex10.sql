SELECT P1.name AS person_name1,
       P2.name AS person_name2,
       P1.address AS common_address
FROM person AS P1
CROSS JOIN person AS P2
WHERE P1.name != P2.name
    AND P1.address = P2.address
    AND P1.id > P2.id
ORDER BY person_name1,
         person_name2,
         common_address;