SELECT
    M.pizza_name AS pizza_name,
    M.price,
    Z.name AS pizzeria_name,
    V.visit_date
FROM person_visits V
JOIN menu M ON V.pizzeria_id = M.pizzeria_id
JOIN person P ON V.person_id = P.id
JOIN pizzeria Z on V.pizzeria_id = Z.id
WHERE P.name = 'Kate' AND price >= 800 AND price <=1000
ORDER BY 1, 2, 3;