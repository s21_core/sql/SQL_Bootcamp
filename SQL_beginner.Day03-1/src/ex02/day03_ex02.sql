SELECT M.pizza_name,
       M.price,
       Z.name as pizzeria_name
from menu M
LEFT JOIN person_order O ON o.menu_id=M.id
JOIN pizzeria Z on Z.id=M.pizzeria_id
WHERE o.id is NULL
ORDER BY 1,
         2;