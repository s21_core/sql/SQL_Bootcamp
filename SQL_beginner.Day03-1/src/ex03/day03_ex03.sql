with cte_male_feemale as 
    (SELECT z.name, P.gender FROM person_visits V
    JOIN person P on P.id=v.person_id
    JOIN pizzeria Z on Z.id=V.pizzeria_id)

Select name FROM
(SELECT name, 
sum(case when gender='male' then 1 else 0 end) as male,
sum(case when gender='female' then 1 else 0 end) as female
FROM cte_male_feemale
GROUP BY name) as R
WHERE male<>female
ORDER BY 1;