with cte_male as 
    (SELECT DISTINCT z.name, P.gender FROM person_order V
    JOIN person P on P.id=v.person_id
    JOIN menu M on V.menu_id=M.id
    JOIN pizzeria Z on Z.id=M.pizzeria_id
    WHERE P.gender='male')
,
cte_female as 
    (SELECT DISTINCT z.name, P.gender FROM person_order V
    JOIN person P on P.id=v.person_id
    JOIN menu M on V.menu_id=M.id
    JOIN pizzeria Z on Z.id=M.pizzeria_id
    WHERE P.gender='female')

SELECT name as pizzeria_name from cte_female
EXCEPT
SELECT name as pizzeria_name from cte_male
ORDER BY pizzeria_name;

