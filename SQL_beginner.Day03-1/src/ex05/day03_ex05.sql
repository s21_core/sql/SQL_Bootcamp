SELECT Z.name as pizzeria_name
FROM person_visits V
FULL JOIN person_order O ON o.order_date = v.visit_date
JOIN person P On P.id=V.person_id
JOIN pizzeria Z on z.id=v.pizzeria_id
WHERE P.name='Andrey'
    AND O.id is NULL
ORDER BY pizzeria_name;