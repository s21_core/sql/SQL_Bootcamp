with cte_name_and_price as 
    (SELECT M.pizza_name, M.price, Z.name,Z.id From menu M 
    JOIN pizzeria Z ON Z.id=M.pizzeria_id)

SELECT C1.pizza_name,C1.name as pizza_name_1, C2.name as pizza_name_2, C1.price from cte_name_and_price C1
JOIN cte_name_and_price C2 ON C2.pizza_name=C1.pizza_name
WHERE C2.id>C1.id AND C2.price=C1.price
ORDER BY 1;