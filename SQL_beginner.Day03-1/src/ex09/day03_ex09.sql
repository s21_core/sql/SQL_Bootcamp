INSERT INTO person_visits(id,visit_date,person_id,pizzeria_id) 
VALUES((SELECT MAX(id) FROM person_visits)+1,
        '2022-02-24',
        (SELECT id FROM person WHERE name='Denis'),
        (SELECT id FROM pizzeria WHERE name='Dominos')
        );

INSERT INTO person_visits(id,visit_date,person_id,pizzeria_id) 
VALUES((SELECT MAX(id) FROM person_visits)+1,
        '2022-02-24',
        (SELECT id FROM person WHERE name='Irina'),
        (SELECT id FROM pizzeria WHERE name='Dominos')
        );

-- select * from person_visits
-- where id>19
-- ORDER BY id;