SELECT generated_date AS missing_date FROM v_generated_dates VGD
FULL JOIN person_visits V on VGD.generated_date=V.visit_date
WHERE id is NULL
ORDER BY missing_date
