CREATE VIEW v_symmetric_union AS
    (WITH V02 AS 
    (SELECT person_id FROM person_visits V
    WHERE V.visit_date='2022-01-02'),
    V06 AS 
    (SELECT person_id FROM person_visits V
    WHERE V.visit_date='2022-01-06')
    
    (SELECT * FROM V02
    EXCEPT
    SELECT * FROM V06)

    UNION

    (SELECT * FROM V06
    EXCEPT
    SELECT * FROM V02)
    ORDER BY person_id);

-- SELECT * FROM v_symmetric_union;