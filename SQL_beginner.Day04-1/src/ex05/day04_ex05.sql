CREATE VIEW v_price_with_discount AS
    (SELECT p.name, M.pizza_name, M.price, ROUND(M.price *0.9,0) as discount_price FROM person_order O
    JOIN person P ON p.id=O.person_id
    JOIN menu M ON M.id=O.menu_id
    ORDER BY name, pizza_name)

-- SELECT * FROM v_price_with_discount;