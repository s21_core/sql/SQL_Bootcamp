CREATE MATERIALIZED VIEW mv_dmitriy_visits_and_eats AS
    (SELECT Z.name as pizzeria_name
    FROM person_visits V
    JOIN person P ON V.person_id = P.id
    JOIN menu M ON V.pizzeria_id = M.pizzeria_id
    JOIN pizzeria Z ON M.pizzeria_id= Z.id
    WHERE P.name = 'Dmitriy'
        AND V.visit_date = '2022-01-08'
        AND M.price < 800);

-- SELECT * FROM mv_dmitriy_visits_and_eats;