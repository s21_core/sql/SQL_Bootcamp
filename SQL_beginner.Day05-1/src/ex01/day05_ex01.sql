SET enable_seqscan TO ON;
EXPLAIN ANALYZE
    SELECT M.pizza_name, Z.name FROM menu M
    JOIN pizzeria Z on M.pizzeria_id= Z.id
    ORDER BY 1,2 DESC;

SET enable_seqscan TO OFF;
EXPLAIN ANALYZE
    SELECT M.pizza_name, Z.name FROM menu M
    JOIN pizzeria Z on M.pizzeria_id= Z.id
    ORDER BY 1,2 DESC