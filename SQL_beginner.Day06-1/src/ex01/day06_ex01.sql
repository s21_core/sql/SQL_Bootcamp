INSERT INTO person_discounts(id, person_id, pizzeria_id, discount_percent)
SELECT ROW_NUMBER() OVER() AS id, O.person_id, M.pizzeria_id,
     (
        CASE
        WHEN
            COUNT(O.person_id) = 1 THEN 10.5 
        WHEN
            COUNT(O.person_id) = 2 THEN 22
        ELSE
           30
        END
     )as discount_percent
FROM person_order O
JOIN menu M ON O.menu_id=M.id
GROUP BY O.person_id, M.pizzeria_id
ORDER BY person_id;

SELECT P.name,Z.name as pizzeria,discount_percent FROM person_discounts D
JOIN pizzeria Z ON z.id =D.pizzeria_id
JOIN person P On P.id=D.person_id;
