SELECT P.name,
    M.pizza_name,
    M.price,
    ROUND(M.price *(100 - D.discount_percent) / 100, 2) as discount_price,
    Z.name as pizzeria_name
FROM person_order O
    JOIN menu M ON O.menu_id=M.id
    JOIN pizzeria Z ON z.id = M.pizzeria_id
    JOIN person_discounts D ON D.pizzeria_id = Z.id AND O.person_id = D.person_id
    JOIN person P On P.id = D.person_id
ORDER BY 1,2