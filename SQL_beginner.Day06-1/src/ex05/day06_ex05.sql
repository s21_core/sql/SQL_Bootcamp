COMMENT ON TABLE person_discounts IS 'Table contains information about personal discounts of customers at corresponding pizzerias';
COMMENT ON COLUMN person_discounts.id IS 'Unique id, PRIMARY KEY';
COMMENT ON COLUMN person_discounts.person_id IS 'Unique id, FOREIGIN KEY from persons table';
COMMENT ON COLUMN person_discounts.pizzeria_id IS 'Unique id, FOREIGIN KEY from pizzrias table, pizzeria where the person has corresponding discount';
COMMENT ON COLUMN person_discounts.discount IS 'Numeric value(7,2) of persons discount at corresponding pizzeria';
