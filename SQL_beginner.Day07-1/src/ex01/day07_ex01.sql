SELECT P.name, COUNT(*) AS count_of_visits
FROM person_visits V
    JOIN person P ON P.id=V.person_id
GROUP BY P.name
ORDER BY 2 DESC,1
LIMIT 4;