WITH top_orders AS
    (SELECT Z.name, count(*) count, 'order' as action_type
    FROM person_order O
        JOIN menu M ON M.id=O.menu_id
        JOIN pizzeria Z on Z.id=M.pizzeria_id
    GROUP BY Z.name
    ORDER BY 2 DESC, 1),
    top_visits AS
    (SELECT Z.name, count(*) count, 'visit' as action_type
    FROM person_visits V
        JOIN pizzeria Z on Z.id=V.pizzeria_id
    GROUP BY Z.name
    ORDER BY 2 DESC, 1)
SELECT O.name, (O.count + V.count) as total_count FROM top_orders O
JOIN top_visits V ON O.name=V.name
ORDER BY 2 DESC,1 ASC;