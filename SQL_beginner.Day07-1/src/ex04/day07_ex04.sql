SELECT P.name, COUNT(*) as count_of_visits
FROM person_visits V
    JOIN person P on P.id = V.person_id
GROUP BY p.id
HAVING COUNT(*) > 3;