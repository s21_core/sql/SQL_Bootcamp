SELECT Z.name, COUNT(*) as count_of_orders, ROUND(AVG(M.price),2) as average_price, ROUND(MAX(M.price),2) as max_price, ROUND(MIN(M.price),2) as min_price 
FROM person_order O
    JOIN menu M ON M.id=O.menu_id
    JOIN pizzeria Z on Z.id=M.pizzeria_id
GROUP BY Z.name 
ORDER BY 1 ;