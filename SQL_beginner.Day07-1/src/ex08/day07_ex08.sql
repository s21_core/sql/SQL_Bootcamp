SELECT P.address,Z.name,COUNT(Z.name) as count_of_orders FROM person_order O
    JOIN person P ON P.id=O.person_id
    JOIN menu M ON M.id=O.menu_id
    JOIN pizzeria Z ON Z.id=M.pizzeria_id   
GROUP BY P.address,Z.name
ORDER BY 1,2;