WITH clac AS
(SELECT P.address,
    ROUND((MAX(age) - (MIN(age) *1.0 / MAX(age))),2) as formula,
    ROUND(AVG(P.age), 2) AS average
FROM person P
GROUP BY P.address
ORDER BY 1)
SELECT *,
CASE WHEN formula > average then True ELSE False END
FROM clac;