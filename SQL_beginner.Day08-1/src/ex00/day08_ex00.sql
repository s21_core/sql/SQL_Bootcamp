-- Session #1
BEGIN TRANSACTION;
SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';
UPDATE pizzeria SET rating = 5 WHERE name = 'Pizza Hut';
SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';
-- Session #2
SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';
-- Session #1
COMMIT TRANSACTION;
-- Session #2
SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';