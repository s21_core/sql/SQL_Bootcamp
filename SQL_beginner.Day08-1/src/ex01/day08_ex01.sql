-- Session #1
BEGIN TRANSACTION;
SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';
UPDATE pizzeria SET rating = 4 WHERE name = 'Pizza Hut';
SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';
-- Session #2
BEGIN TRANSACTION;
SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';
UPDATE pizzeria SET rating = 3.6 WHERE name = 'Pizza Hut';
SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';
-- Session #1
COMMIT TRANSACTION;
-- Session #2
COMMIT TRANSACTION;

SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';