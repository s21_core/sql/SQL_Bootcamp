-- Session #1
BEGIN TRANSACTION ISOLATION LEVEL READ COMMITTED;
-- Session #2
BEGIN TRANSACTION ISOLATION LEVEL READ COMMITTED;
-- Session #1
SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';
-- Session #2
UPDATE pizzeria SET rating = 3.6 WHERE name = 'Pizza Hut';
COMMIT;
-- Session #1
SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';
COMMIT;
SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';
-- Session #2
SELECT name,rating FROM pizzeria Z WHERE name = 'Pizza Hut';