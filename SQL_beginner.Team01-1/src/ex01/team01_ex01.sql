-- insert into currency values (100, 'EUR', 0.85, '2022-01-01 13:29');
-- insert into currency values (100, 'EUR', 0.79, '2022-01-08 13:29');

CREATE OR REPLACE FUNCTION nearest_rate(p_cur_id bigint, p_date timestamp) RETURNS numeric AS $$
DeCLARE res numeric = NULL;
begin
SELECT rate_to_usd INTO res
FROM (
        (
            SELECT rate_to_usd,
                (c.updated - p_date) as time_diff
            FROM currency C
            WHERE c.updated > p_date
                and C.id = p_cur_id
            ORDER BY time_diff
            LIMIT 1
        )
        UNION
        (
            SELECT rate_to_usd,
                (p_date - c1.updated) as time_diff
            FROM currency C1
            WHERE c1.updated < p_date
                and C1.id = p_cur_id
            ORDER BY time_diff
            LIMIT 1
        )
        ORDER BY time_diff ASC
        LIMIT 1
    ) R;
raise notice 'The number of %', res;
return res;
end;
$$ LANGUAGE plpgsql;

SELECT COALESCE(U.name, 'not defined') AS name
    ,COALESCE(U.lastname, 'not defined') AS lastname
    ,C.name AS currency_name
    ,ROUND (B.money * nearest_rate(B.currency_id, b.updated),2) AS currency_in_usd
    ,nearest_rate(B.currency_id, b.updated) as nearest_rate
FROM balance B
    LEFT JOIN "user" U ON B.user_id=U.id
    LEFT JOIN (SELECT DISTINCT id, name from currency) C ON c.id = B.currency_id
    WHERE C.name is not NULL
ORDER BY 1 DESC, 2, 3;

-- name
--     source: user.name if user.name is NULL then return not defined value
-- lastname
--     source: user.lastname if user.lastname is NULL then return not defined value
-- currency_name
--     source: currency.name
-- currency_in_usd
--     involved sources: currency.rate_to_usd, currency.updated, balance.updated.Take a look at a graphical interpretation of the formula below.
--     need to find a nearest rate_to_usd of currency at the past (t1)
--     if t1 is empty (means no any rates at the past) then find a nearest rate_to_usd of currency at the future (t2)
--     use t1 OR t2 rate to calculate a currency in USD format