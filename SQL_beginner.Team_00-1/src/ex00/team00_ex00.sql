WITH 
	all_tours AS
	(SELECT
		tc1.cost + tc2.cost + tc3.cost + tc4.cost AS total_cost,
		'{' || tc1.p1 || ',' || tc1.p2 || ',' || tc2.p2 || ',' || tc3.p2 || ',' || tc4.p2 || '}' AS tour
	FROM
		nodes AS tc1
		JOIN nodes tc2 ON tc1.p2 = tc2.p1
		JOIN nodes tc3 ON tc2.p2 = tc3.p1
		JOIN nodes tc4 ON tc3.p2 = tc4.p1
	WHERE
		tc1.p1 = 'd'
		AND tc2.p2 <> tc1.p1
		AND tc3.p2 <> tc1.p1 
		AND tc3.p2 <> tc2.p1
		AND tc4.p2 = tc1.p1
	ORDER BY
		total_cost,
		tour)
SELECT
	*
FROM
	all_tours


